//
//  ViewController.swift
//  Assignment2Swift
//
//  Created by Jason Clinger on 1/23/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
//    @IBAction func btn1Touched(sender: UIButton){}
    
    
    @IBOutlet weak var label1: UITextField!
    
    @IBOutlet weak var label2: UITextField!
    
    @IBOutlet weak var txtfld1: UILabel!
    
    @IBOutlet weak var txtfld2: UILabel!
    
    @IBOutlet weak var btn1: UIButton!
    
    @IBAction func btn1Touched(sender: UIButton) {
        NSLog("scores reset", "")
        stepper1.value = 0;
        stepper2.value = 0;
        txtfld1.text = String(format:"%.f", stepper1.value);
        txtfld2.text = String(format:"%.f", stepper2.value);
    }
    
    @IBOutlet weak var stepper1: UIStepper!
    
    @IBOutlet weak var stepper2: UIStepper!
    
    @IBAction func stepper1Touched(sender: UIStepper) {
        NSLog("%.f", stepper1.value)
        txtfld1.text = String(format:"%.f", stepper1.value);
    }
    
    @IBAction func stepper2Touched(sender: UIStepper) {
        NSLog("%.f", stepper2.value)
        txtfld2.text = String(format:"%.f", stepper2.value);
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        let view : UIView = UIView(frame: CGRectMake(100,100,100,100))
//        view.backgroundColor = UIColor.greenColor();
//        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth;
//        self.view.addSubview(view);
        
        
    }
    
    
//    @IBAction func stepOn(sender: UIStepper){
//    NSLog("%f",sender.value)
//    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(animated)
        txtfld1.text = String(format:"%.f", stepper1.value);
        txtfld2.text = String(format:"%.f", stepper2.value);
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

